import React from "react";
import "./home.css";

// import "@ionic/react/css/core.css";
import "primeicons/primeicons.css";
import NotificationIcon from "./notification";

export default function Home() {
  return (
    <div className="home">
      <div className="header-icon">
        <NotificationIcon /></div>
      <div className="team-logo">
        <img
          src="./assets/logo.png"
          alt=""
          style={{ height: 150, width: 270 }}
        />
        <h1 className="team-name">TERRASAVIORS</h1>
      </div>
      <footer>
        <h5 className="footer-style">Sensing Earth, Saving Lives ...</h5>
      </footer>
    </div>
  );
}
