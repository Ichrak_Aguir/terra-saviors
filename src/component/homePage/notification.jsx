import React, { useState } from 'react';
import { IonIcon } from "@ionic/react";
import { notificationsOutline } from "ionicons/icons";

import './NotificationIcon.css'; 
import { Link } from 'react-router-dom';

function NotificationIcon() {
  const [showNotifications, setShowNotifications] = useState(false);
  const notifications = [
    " Detection n°1 : 35.73163030947738, 10.819525882366554"
  ];

  const toggleNotifications = () => {
    setShowNotifications(!showNotifications);
  };

  return (
    <div className="notification-icon-container">
      <div className="notification-icon" onClick={toggleNotifications}>
        <span className="notification-badge">{notifications.length}</span>
        <IonIcon
          className="notification-icon"
          icon={notificationsOutline}
        ></IonIcon>
      </div>
      {showNotifications && (
        <div className="notification-list">
          <h3>Notifications</h3>
          <ul>
            {notifications.map((notification, index) => (
              <li key={index}><Link to="/35.73163030947738, 10.819525882366554" className="link" >{notification}</Link></li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default NotificationIcon;
