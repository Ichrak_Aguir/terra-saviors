import React from 'react';
import './alert.css';
import { Link } from 'react-router-dom';

function Alert() {
  return (
    <div className="user-body">
      <div className="alert-header">
        <h3 >ALERT #342367</h3>
        <Link to="/" className="link" ><img src="./assets/logo.png" alt="TS-logo" style={{ height: 45, width: 55 }} /></Link>
        
      </div>
      <div className="alert-main-video">
        <img src="./assets/gif.gif" alt="main-vid" className="gif" />
        <div className="alert-description">
        <p><strong>Geographic coordinates: </strong>35.73163030947738, 10.819525882366554</p>
        <p><strong>Date & time: </strong>17/12/2022, 19:02:44</p>
        <p><strong>Event Description: </strong>Earthquake</p>
        <p><strong>Severity Level: </strong>High</p>
      </div>
      </div>
      <div className="alert-detailed-videos">
      <video controls autoplay className='video-css' >
        <source src="./assets/video.mp4" type="video/mp4" />
      </video>
      </div>
      
    </div>
  )
}

export default Alert;
