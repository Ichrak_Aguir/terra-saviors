import { BrowserRouter , Route,Routes } from 'react-router-dom';
import Home from './component/homePage/home';
import Alert  from './component/userPage/Alert';
function App() {
  return (
    <BrowserRouter>
    
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/35.73163030947738, 10.819525882366554" element={<Alert />} />

        </Routes>

    </BrowserRouter>
  );
}

export default App;

